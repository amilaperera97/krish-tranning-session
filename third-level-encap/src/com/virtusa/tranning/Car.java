package com.virtusa.tranning;

public class Car {
	
	private Double id;
	private String color;
	private String wheelType;
	
	public Double getId() {
		return id;
	}
	public void setId(Double id) {
		this.id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getWheelType() {
		return wheelType;
	}
	public void setWheelType(String wheelType) {
		this.wheelType = wheelType;
	}
	
	public class PaintStation {

		Boolean flag=false;
		
		public boolean validColor(Car car) {
			switch(car.getColor().toUpperCase()) {
				case "RED":
					flag=true;
					break;
				case "GREEN":
					flag=true;
					break;
				case "BLUE":
					flag=true;
					break;
				case "WHITE":
					flag=true;
					break;
					
			}
			return flag;
		}
	}

	/**
	 * INNER CLASS
	 * @param wheelType
	 * @return
	 */
	public String wheelType(String wheelType) {
		
		class WheelValidator {
			public WheelValidator(String wheelType) {
				switch(wheelType.toUpperCase()) {
					case "SMALL":
						System.out.println("Selected type is SMALL!");
						break;
					case "MEDIUM":
						System.out.println("Selected type is MEDIUM!");
						break;
					case "LARGE":
						System.out.println("Selected type is LARGE!");
						break;
					default:
						System.out.println("Selected type is DEFAULT!");
						break;
				}
			}
		}
		
		new WheelValidator(wheelType);
		return null;
	}
}
